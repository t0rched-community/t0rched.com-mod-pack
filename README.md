![logo_dark.png](https://bitbucket.org/repo/AKzpzb/images/3416148917-logo_dark.png)
# Welcome to the t0rched.com Community Repositories! #

The repository packs are pretty straight forward to use. You can request permission to access the repo, just register a **FREE** BitBucket account.

This pack contains every needed to build the custom launcher.
